import log4js from 'log4js'

interface CodeType {
  encode: (str: string) => string
  decode: (str: string) => string
}

const hex2byte = (str: string): number[] => {
  let pos = 0
  let len = str.length
  if (len % 2 != 0) {
    return new Array()
  }
  len /= 2
  let hexA = new Array()
  for (let i = 0; i < len; i++) {
    let s = str.substr(pos, 2)
    let v = parseInt(s, 16)
    hexA.push(v)
    pos += 2
  }
  return hexA
}
const calcDecryptTable = (arr: number[]) => {
  let $rtn = new Array(256)
  for (var i = 0; i < 256; i++) {
    $rtn[arr[i]] = i
  }

  return $rtn
}

const ENC_TABLE =
  '66D8F5A77155FB7EE35CC9AFCB85B6F06537350C4DE86D190BE769106144501B9502D1A6DD05073BB77B1DC6DA2CF8158ED560D04E7F3DFFC73F992BA86ACC4C095D57F62FB4F2F4568793CE4276735241631E36FEDCACC862981ABADE332D25C21301F76B16757AEE6CBCC59DFC1824CDDB38E4AADFBE23F19AE2320DEA0091D91C5E794A880F94F9CFB50E6481ED89D3479CA08F3C034B54A58AA158F326B3C428744F3097CAAB720A5A7CD7686EB8902EE014B1FD27D28CEB92A9C3458021A2AE461FBD0678B2B912EC775F4311C0A38DD69B838B96485BE13A82BF4039290849347067FA9E3186D4B02A84A4EF53BBC1AD7D04E9515920E56F179FE6223E'
// @ts-ignore
const meta_ori = hex2byte(ENC_TABLE)
const meta_dec = calcDecryptTable(meta_ori)

const tableConvert = (table: number[], val: number): number => {
  return table[val & 0xff]
}

const tConvert = (line: number, val: number): number => {
  //n + 1行解密 判断奇偶 奇数行 偶数解密
  return tableConvert((line & 1) === 1 ? meta_ori : meta_dec, val)
}

const convertBase64 = (value: string, codeType: 'encode' | 'decode'): string => {
  const convertMap: CodeType = {
    encode(str: string): string {
      return btoa(unescape(encodeURIComponent(str)))
    },
    decode(str: string): string {
      return decodeURIComponent(escape(atob(str)))
    },
  }
  return convertMap[codeType](value)
}

const convertTableHex = (value: string, codeType: 'encode' | 'decode'): string => {
  const convertMap: CodeType = {
    encode(str: string): string {
      return Buffer.from(str, 'utf-8').toString('hex')
    },
    decode(str: string): string {
      let $hex = Buffer.from(str, 'hex')
      //大端无符号 加密行
      console.log($hex)
      let $len = $hex.length
      console.log('tb len->' + $len)
      let $result = new Array($hex.length - 2)
      let $line = $hex.readUInt16BE(0)
      console.log('tb line->' + $line)
      for (var i = 2; i < $len; i++) {
        $result.push(tConvert($line, $hex.readUInt8(i)))
      }

      return Buffer.from($result).toString('utf-8')
      // return $line + '-' + $hex
    },
  }
  return convertMap[codeType](value)
}

const convertHex = (value: string, codeType: 'encode' | 'decode'): string => {
  const convertMap: CodeType = {
    encode(str: string): string {
      return Buffer.from(str, 'utf-8').toString('hex')
    },
    decode(str: string): string {
      return Buffer.from(str, 'hex').toString('utf-8')
    },
  }
  return convertMap[codeType](value)
}

const convertJSON = (value: string): Promise<string> =>
  new Promise((resolve, reject) => {
    try {
      let $json = JSON.parse(value)
      $json = JSON.stringify($json, null, 2)
      return resolve($json)
    } catch (error) {
      return reject(error)
    }
  })

const convertPayload = async (payload: string, currentType: PayloadType, fromType: PayloadType): Promise<string> => {
  let $payload = payload
  switch (fromType) {
    case 'Base64':
      $payload = convertBase64(payload, 'decode')
      break
    case 'Hex':
      $payload = convertHex(payload, 'decode')
      break
    case 'TableHex':
      $payload = convertTableHex(payload, 'decode')
      break
  }
  if (currentType === 'Base64') {
    $payload = convertBase64($payload, 'encode')
  }
  if (currentType === 'JSON') {
    $payload = await convertJSON($payload)
  }
  if (currentType === 'Hex') {
    $payload = convertHex($payload, 'encode')
  }
  if (currentType === 'TableHex') {
    $payload = convertTableHex($payload, 'encode')
  }
  return $payload
}

export default { convertPayload, convertTableHex }
